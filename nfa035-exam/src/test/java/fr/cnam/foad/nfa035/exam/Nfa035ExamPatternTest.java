package fr.cnam.foad.nfa035.exam;


import fr.cnam.foad.nfa035.exam.model.MyExam;
import fr.cnam.foad.nfa035.exam.model.MyExamSortingWrapper;
import fr.cnam.foad.nfa035.exam.pattern.MyExamDelegate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ContextConfiguration(classes = { Nfa035ExamConfiguration.class })
@RunWith(SpringRunner.class)
@SpringBootTest
public class Nfa035ExamPatternTest {

    @Autowired
    MyExamDelegate delegate;

    @Autowired
    MyExam currentExam;

    /**
     * Documentez-moi
     *
     * @throws Exception
     */
    @Test
    public void testDelegateToString() throws Exception {
        String toString = delegate.delegateToString();
        System.out.println(toString);
        assertEquals(currentExam.toString(), toString);
    }


    /**
     * Documentez-moi
     *
     * @throws Exception
     */
    @Test
    public void testWrapperPattern() throws Exception {
        MyExam exam = new MyExam("Bibliotheques et Patterns", "NFA035", "CNAM", "Ile de France", new Date(System.currentTimeMillis()));
        MyExamSortingWrapper wrapper = new MyExamSortingWrapper(exam);
        assertEquals(wrapper.getExam(), exam);
    }

    /**
     * Documentez-moi
     *
     * @throws Exception
     */
    @Test
    public void testWrapperPatternToString() throws Exception {
        MyExam exam = new MyExam("Bibliotheques et Patterns", "NFA035", "CNAM", "Ile de France", new Date(System.currentTimeMillis()));
        MyExamSortingWrapper wrapper = new MyExamSortingWrapper(exam);
        System.out.println(wrapper.toString());
        assertTrue(wrapper.toString().contains(wrapper.getExam().toString()));
    }

    /**
         * Documentez-moi
         *
         * @throws Exception
         */
    @Test
    public void testCompareToBuilder() throws Exception {
        MyExam exam1 = new MyExam("Bibliotheques et Patterns", "NFA035", "CNAM", "Ile de France", new Date(System.currentTimeMillis()));
        MyExam exam2 = new MyExam("Bibliotheques et Patterns", "NFA035", "CNAM", "Est", new Date(System.currentTimeMillis()+1));
        MyExam exam3 = new MyExam("Bibliotheques et Patterns", "NFA035", "CNAM", "Centre", new Date(System.currentTimeMillis()+2));
        MyExam exam4 = new MyExam("Java basic", "NFA031", "CNAM", "Ile de France", new Date(System.currentTimeMillis()+3));
        MyExam exam5 = new MyExam("Java Objet", "NFA032", "CNAM", "Ile de France", new Date(System.currentTimeMillis()+4));

        Set<MyExamSortingWrapper> exams = new TreeSet<MyExamSortingWrapper>();
        exams.add(new MyExamSortingWrapper(exam5));
        exams.add(new MyExamSortingWrapper(exam4));
        exams.add(new MyExamSortingWrapper(exam3));
        exams.add(new MyExamSortingWrapper(exam2));
        exams.add(new MyExamSortingWrapper(exam1));

        System.out.println(exams);

        MyExamSortingWrapper[] examTable = exams.toArray(new MyExamSortingWrapper[]{});
        assertEquals(exam1,examTable[4].getExam());
        assertEquals(exam2,examTable[3].getExam());
        assertEquals(exam3,examTable[2].getExam());
        assertEquals(exam4,examTable[0].getExam());
        assertEquals(exam5,examTable[1].getExam());
    }


}