package fr.cnam.foad.nfa035.exam.model;

import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class MyExamSortingWrapper implements Comparable<MyExamSortingWrapper>{

    private MyExam exam;

    public MyExamSortingWrapper(MyExam exam) {
        this.exam = exam;
    }

    public MyExam getExam() {
        return exam;
    }

    public void setExam(MyExam exam) {
        this.exam = exam;
    }

    @Override
    public int compareTo(MyExamSortingWrapper o) {
        return new CompareToBuilder()
                .append(this.exam.getCodeUe(), o.exam.getCodeUe())
                .append(this.exam.getRegion(), o.exam.getRegion())
                .append(this.exam.getDate(), o.exam.getDate())
                .toComparison();
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
